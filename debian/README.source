This package follows the Debian OTR Team's workflow and policy,
as documented on https://wiki.debian.org/Teams/OTR.

This package ships a shared library. So, when upgrading the package:

* dpkg-gensymbols may warn about changed symbols; in this case, update
  debian/*.symbols; for more information, see
  https://www.debian.org/doc/manuals/maint-guide/advanced.en.html#librarysymbols

* Manually diff the header files between the previous version and the
  new one, and look for incompatible ABI changes that dpkg-gensymbols
  cannot detect, such as changes in an enum or struct. Then, bump the
  needed version in the symbols file for every function that uses one
  of the data structure that has changed.
